package com.example.alvi.romoniwithfragment.Interfaces;

import android.support.v4.app.Fragment;

public interface FragmentChangeListener {
    public void replaceFragment(Fragment fragment);

}
