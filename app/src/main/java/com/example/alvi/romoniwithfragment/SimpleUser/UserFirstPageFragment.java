package com.example.alvi.romoniwithfragment.SimpleUser;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;

import com.example.alvi.romoniwithfragment.Adapters.RecyclerAdapter;
import com.example.alvi.romoniwithfragment.BussinessUser.BusinessUserSignUpFragment;
import com.example.alvi.romoniwithfragment.Classes.GetterSetter;
import com.example.alvi.romoniwithfragment.Interfaces.FragmentChangeListener;
import com.example.alvi.romoniwithfragment.Interfaces.OnItemClickedFromAdapter;
import com.example.alvi.romoniwithfragment.R;

import java.util.ArrayList;


public class UserFirstPageFragment extends Fragment implements OnItemClickedFromAdapter {

    private static final String COMMON_TAG = "CombinedLifeCycle";
    private static final String FRAGMENT_NAME = UserLogInFragment.class.getSimpleName();

    private static final String TAG = FRAGMENT_NAME;

    View view;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private RecyclerAdapter mAdapter;
    private Context context;

    private ArrayList<GetterSetter> feedItemList;

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;

    ConstraintLayout drawerView,mainView,dropSign,dropLog;

    ConstraintLayout signlog1,signlog2,signlog3,signlog5,signlog6,signLay,logLay,forgetLay;

    Button button,button2,cross,setUpSignButton,clientSignButton,drawerSign,drawerLogin;
    TextView loginInWhite,signUpInwhite,forgetpassdrop;
    View signview,loginview;
    String check;



    ViewGroup hiddenPanel,mainPanel;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.i(TAG, FRAGMENT_NAME +" onAttach");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, FRAGMENT_NAME +" onCreate");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        Log.i(TAG, FRAGMENT_NAME +" onCreateView");
        view =inflater.inflate(R.layout.fragment_user_first_page,container,false);




        hiddenPanel = (ViewGroup)view.findViewById(R.id.pop);
        mainPanel = (ViewGroup)view.findViewById(R.id.rest);

        mDrawerLayout = (DrawerLayout) view.findViewById(R.id.drawer_layout);
        drawerView = (ConstraintLayout) view.findViewById(R.id.drawerView);
        mainView = (ConstraintLayout) view.findViewById(R.id.maincons);
        signlog1 = (ConstraintLayout) view.findViewById(R.id.one);
        signlog2 = (ConstraintLayout) view.findViewById(R.id.two);
        signlog3 = (ConstraintLayout) view.findViewById(R.id.three);
        signlog5 = (ConstraintLayout) view.findViewById(R.id.five);
        signlog6 = (ConstraintLayout) view.findViewById(R.id.six);


        dropSign = (ConstraintLayout) view.findViewById(R.id.drop1);
        dropLog = (ConstraintLayout) view.findViewById(R.id.drop2);
        setUpSignButton =  view.findViewById(R.id.set_up);
        clientSignButton = view.findViewById(R.id.set_up2);
        drawerSign = view.findViewById(R.id.drawer_signup);
        drawerLogin = view.findViewById(R.id.drawer_login);

        button =  view.findViewById(R.id.btn_menu);
        button2 =  view.findViewById(R.id.btn_search);

        //find recyclerview layout
        mRecyclerView = (RecyclerView)  view.findViewById(R.id.recyclerview);
        mRecyclerView.setHasFixedSize(true);

        // recyclerview set layoutmanager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        loginInWhite =  view.findViewById(R.id.logT);
        signUpInwhite=  view.findViewById(R.id.signT);
        signview=  view.findViewById(R.id.vsign);
        loginview=  view.findViewById(R.id.vlog);
        signLay=  view.findViewById(R.id.signlay);
        logLay=  view.findViewById(R.id.loglay);
        forgetpassdrop =  view.findViewById(R.id.forgetpass);
        forgetLay =  view.findViewById(R.id.forgetlay);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        Log.i(TAG, FRAGMENT_NAME +" onActivityCreated");
        super.onActivityCreated(savedInstanceState);

        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            check = bundle.getString("flag");
        }


        if(check==null)
            check="";
        mDrawerLayout.setScrimColor(getResources().getColor(android.R.color.transparent));
        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), mDrawerLayout, R.string.open,R.string.close) {
            public void onDrawerClosed(View view) {
                getActivity().supportInvalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                getActivity().supportInvalidateOptionsMenu();
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                mainView.setTranslationX(slideOffset * drawerView.getWidth());
                mDrawerLayout.bringChildToFront(drawerView);
                mDrawerLayout.requestLayout();
            }
        };
        mDrawerLayout.addDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mDrawerLayout.openDrawer(Gravity.LEFT);
            }
        });

        int[] list_images={
                R.drawable.r10,
                R.drawable.r8,
                R.drawable.r6,
                R.drawable.r7,
                R.drawable.r9
        };

        String[] list_header={
                "1st page header",
                "2nd page header a big one to test",
                "3rd page header even bigger one to test it to the bone",
                "4th page header",
                "5th page header"


        };

        String[] list_body={
                "1st page body",
                "2nd page body a big one to test",
                "3rd page body even bigger one to test it to the bone",
                "4th page body",
                "5th page body"

        };
        //if logged, then eliminating 1st login/sign up view
        int num;
        if(check.equals("logged"))
        {
            num=3;
            signlog1.setVisibility(View.GONE);
            signlog2.setVisibility(View.GONE);
            signlog3.setVisibility(View.GONE);
            signlog5.setVisibility(View.VISIBLE);
            signlog6.setVisibility(View.VISIBLE);
        }

        else
            num=4;

        //adding data to arraylist
        feedItemList = new ArrayList<GetterSetter>();
        for (int i = 0; i < num+5; i++) {
            GetterSetter getterSetter = new GetterSetter();
            if(i>=num){
                getterSetter.setHeader(list_header[i-num]);
                getterSetter.setBody(list_body[i-num]);
                getterSetter.setId(list_images[i-num]);
            }

            feedItemList.add(getterSetter);
        }

        //recyclerview adapter
        mAdapter = new RecyclerAdapter(getActivity(), feedItemList,this,check);

        //set adpater for recyclerview
        mRecyclerView.setAdapter(mAdapter);

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                int firstVisiblePosition = mLayoutManager.findFirstVisibleItemPosition();
                if(firstVisiblePosition>=2)
                {
                    makeVisible();

                }
                else{
                    button2.setVisibility(View.INVISIBLE);

                }


            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);


            }
        });


        hiddenPanel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideThePop();
            }
        });


        setUpSignButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment=new BusinessUserSignUpFragment();
                FragmentChangeListener fc=(FragmentChangeListener)getActivity();
                fc.replaceFragment(fragment);
            }
        });


        clientSignButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dropLog.setVisibility(View.VISIBLE);
                dropSign.setVisibility(View.GONE);
                loginInWhite.setTextColor(Color.parseColor("#a9a3a3"));
                loginview.setVisibility(View.INVISIBLE);
                signUpInwhite.setTextColor(Color.parseColor("#000000"));
                signview.setVisibility(View.VISIBLE);
                logLay.setVisibility(View.GONE);
                signLay.setVisibility(View.VISIBLE);
                forgetLay.setVisibility(View.GONE);
            }
        });


        drawerSign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDrawerLayout.closeDrawer(Gravity.LEFT);
                Animation bottomUp = AnimationUtils.loadAnimation(getActivity(),
                        R.anim.top_down);
                Animation fadeOut = AnimationUtils.loadAnimation(getActivity(),
                        R.anim.fade_out);
                hiddenPanel.startAnimation(bottomUp);
                hiddenPanel.setVisibility(View.VISIBLE);
                dropSign.setVisibility(View.VISIBLE);
                dropLog.setVisibility(View.GONE);
                mainPanel.startAnimation(fadeOut);
            }
        });



        drawerLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDrawerLayout.closeDrawer(Gravity.LEFT);
                Animation bottomUp = AnimationUtils.loadAnimation(getActivity(),
                        R.anim.top_down);
                Animation fadeOut = AnimationUtils.loadAnimation(getActivity(),
                        R.anim.fade_out);
                hiddenPanel.startAnimation(bottomUp);
                hiddenPanel.setVisibility(View.VISIBLE);
                dropLog.setVisibility(View.VISIBLE);
                dropSign.setVisibility(View.GONE);

                mainPanel.startAnimation(fadeOut);


                loginInWhite.setTextColor(Color.parseColor("#000000"));
                loginview.setVisibility(View.VISIBLE);
                signUpInwhite.setTextColor(Color.parseColor("#a9a3a3"));
                signview.setVisibility(View.INVISIBLE);
                logLay.setVisibility(View.VISIBLE);
                signLay.setVisibility(View.GONE);
                forgetLay.setVisibility(View.GONE);
                //disableEnableControls(false,mainPanel);
            }
        });

        signUpInwhite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginInWhite.setTextColor(Color.parseColor("#a9a3a3"));
                loginview.setVisibility(View.INVISIBLE);
                signUpInwhite.setTextColor(Color.parseColor("#000000"));
                signview.setVisibility(View.VISIBLE);
                logLay.setVisibility(View.GONE);
                signLay.setVisibility(View.VISIBLE);
                forgetLay.setVisibility(View.GONE);

            }
        });

        loginInWhite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginInWhite.setTextColor(Color.parseColor("#000000"));
                loginview.setVisibility(View.VISIBLE);
                signUpInwhite.setTextColor(Color.parseColor("#a9a3a3"));
                signview.setVisibility(View.INVISIBLE);
                logLay.setVisibility(View.VISIBLE);
                signLay.setVisibility(View.GONE);
                forgetLay.setVisibility(View.GONE);

            }
        });

        forgetpassdrop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginInWhite.setTextColor(Color.parseColor("#a9a3a3"));
                loginview.setVisibility(View.INVISIBLE);
                signUpInwhite.setTextColor(Color.parseColor("#a9a3a3"));
                signview.setVisibility(View.INVISIBLE);
                logLay.setVisibility(View.GONE);
                signLay.setVisibility(View.GONE);
                forgetLay.setVisibility(View.VISIBLE);

            }
        });
    }

    @Override
    public void onStart() {
        Log.i(TAG, FRAGMENT_NAME +" onStart");
        super.onStart();
    }

    @Override
    public void onResume() {
        Log.i(TAG, FRAGMENT_NAME +" onResume");
        super.onResume();
    }

    @Override
    public void onPause() {
        Log.i(TAG, FRAGMENT_NAME +" onPause");
        super.onPause();
    }

    @Override
    public void onStop() {
        Log.i(TAG, FRAGMENT_NAME +" onStop");
        super.onStop();
    }


    @Override
    public void onDestroyView() {
        Log.i(TAG, FRAGMENT_NAME +" onDestroyView");
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        Log.i(TAG, FRAGMENT_NAME +" onDestroy");
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        Log.i(TAG, FRAGMENT_NAME +" onDetach");
        super.onDetach();
    }

    public void makeVisible(){
        if (button2.getVisibility() == View.VISIBLE) {
            // do nothing
        } else {
            button2.setVisibility(View.VISIBLE);
        }

    }
    public void hideThePop(){
        if(hiddenPanel.getVisibility() == View.VISIBLE){
            Animation bottomUp = AnimationUtils.loadAnimation(getActivity(),
                    R.anim.top_up);
            Animation fadeIn = AnimationUtils.loadAnimation(getActivity(),
                    R.anim.fade_in);
            hiddenPanel.startAnimation(bottomUp);
            hiddenPanel.setVisibility(View.INVISIBLE);
            mainPanel.startAnimation(fadeIn);
            //disableEnableControls(true,mainPanel);
        }

    }

    @Override
    public void onClick(String value) {
        if(value.equals("sign")){
            Animation bottomUp = AnimationUtils.loadAnimation(getActivity(),
                    R.anim.top_down);
            Animation fadeOut = AnimationUtils.loadAnimation(getActivity(),
                    R.anim.fade_out);
            hiddenPanel.startAnimation(bottomUp);
            hiddenPanel.setVisibility(View.VISIBLE);
            dropSign.setVisibility(View.VISIBLE);
            dropLog.setVisibility(View.GONE);
            mainPanel.startAnimation(fadeOut);


        }
        else if(value.equals("login")){
            Animation bottomUp = AnimationUtils.loadAnimation(getActivity(),
                    R.anim.top_down);
            Animation fadeOut = AnimationUtils.loadAnimation(getActivity(),
                    R.anim.fade_out);
            hiddenPanel.startAnimation(bottomUp);
            hiddenPanel.setVisibility(View.VISIBLE);
            dropLog.setVisibility(View.VISIBLE);
            dropSign.setVisibility(View.GONE);

            mainPanel.startAnimation(fadeOut);


            loginInWhite.setTextColor(Color.parseColor("#000000"));
            loginview.setVisibility(View.VISIBLE);
            signUpInwhite.setTextColor(Color.parseColor("#a9a3a3"));
            signview.setVisibility(View.INVISIBLE);
            logLay.setVisibility(View.VISIBLE);
            signLay.setVisibility(View.GONE);
            forgetLay.setVisibility(View.GONE);
            //disableEnableControls(false,mainPanel);
        }
    }
}