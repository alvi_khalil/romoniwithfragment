package com.example.alvi.romoniwithfragment.SimpleUser;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.alvi.romoniwithfragment.Interfaces.FragmentChangeListener;
import com.example.alvi.romoniwithfragment.R;


public class UserLogInFragment extends Fragment {

    private static final String COMMON_TAG = "CombinedLifeCycle";
    private static final String FRAGMENT_NAME = UserLogInFragment.class.getSimpleName();

    private static final String TAG = FRAGMENT_NAME;

    EditText editEmail,editPass,editEmailForget;
    Button logIn,backButton,getVerificationCodeforget,backButtonforget;
    TextView forget;
    ScrollView scrollViewLogIn,scrollViewForget;
    View view;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.i(TAG, FRAGMENT_NAME +" onAttach");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, FRAGMENT_NAME +" onCreate");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        Log.i(TAG, FRAGMENT_NAME +" onCreateView");
        view =inflater.inflate(R.layout.fragment_user_log_in,container,false);

        editEmail = view.findViewById(R.id.edit_email);
        editPass = view.findViewById(R.id.edit_pass);
        forget = view.findViewById(R.id.textView5);

        logIn = view.findViewById(R.id.create_button);
        backButton = view.findViewById(R.id.black_arrow);
        scrollViewLogIn = view.findViewById(R.id.scroll_login);
        scrollViewForget = view.findViewById(R.id.scroll_forget);
        editEmailForget = view.findViewById(R.id.edit_emailf);
        getVerificationCodeforget = view.findViewById(R.id.create_buttonf);
        backButtonforget = view.findViewById(R.id.black_arrowf);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        Log.i(TAG, FRAGMENT_NAME +" onActivityCreated");
        super.onActivityCreated(savedInstanceState);

        forget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scrollViewLogIn.setVisibility(View.GONE);
                scrollViewForget.setVisibility(View.VISIBLE);
            }
        });
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fragmentManager=(FragmentManager)getFragmentManager();
                fragmentManager.popBackStack();
            }
        });


        logIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (editEmail.getText().toString().equalsIgnoreCase("") || editPass.getText().toString().equalsIgnoreCase("") ) {
                    Toast.makeText(getActivity(), "One or more fields are empty!", Toast.LENGTH_SHORT).show();
                } else {
                    if (!isEmailValid(editEmail.getText().toString())) {
                        Toast.makeText(getActivity(), "Email format is not Right!", Toast.LENGTH_SHORT).show();

                    } else {
                        if (editPass.getText().toString().length() < 7) {
                            Toast.makeText(getActivity(), "Password is not 6+ characters long!", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), "Log in Successful!", Toast.LENGTH_SHORT).show();
                            Bundle bundle = new Bundle();
                            bundle.putString("flag", "logged");
                            Fragment fragment=new UserFirstPageFragment();
                            fragment.setArguments(bundle);
                            FragmentChangeListener fc=(FragmentChangeListener)getActivity();
                            fc.replaceFragment(fragment);


                        }
                    }
                }
            }
        });

        backButtonforget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scrollViewLogIn.setVisibility(View.VISIBLE);
                scrollViewForget.setVisibility(View.GONE);
            }
        });
        getVerificationCodeforget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (editEmailForget.getText().toString().equalsIgnoreCase("")  ) {
                    Toast.makeText(getActivity(), "Field is empty!", Toast.LENGTH_SHORT).show();
                } else {
                    if (!isEmailValid(editEmailForget.getText().toString())) {
                        Toast.makeText(getActivity(), "Email format is not Right!", Toast.LENGTH_SHORT).show();

                    } else {


                        Toast.makeText(getActivity(), "Verification code Sent!", Toast.LENGTH_SHORT).show();
                        FragmentManager fragmentManager=(FragmentManager)getFragmentManager();
                        fragmentManager.popBackStack();

                    }
                }
            }
        });
    }
    boolean isEmailValid(CharSequence email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
    @Override
    public void onStart() {
        Log.i(TAG, FRAGMENT_NAME +" onStart");
        super.onStart();
    }

    @Override
    public void onResume() {
        Log.i(TAG, FRAGMENT_NAME +" onResume");
        super.onResume();
    }

    @Override
    public void onPause() {
        Log.i(TAG, FRAGMENT_NAME +" onPause");
        super.onPause();
    }

    @Override
    public void onStop() {
        Log.i(TAG, FRAGMENT_NAME +" onStop");
        super.onStop();
    }


    @Override
    public void onDestroyView() {
        Log.i(TAG, FRAGMENT_NAME +" onDestroyView");
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        Log.i(TAG, FRAGMENT_NAME +" onDestroy");
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        Log.i(TAG, FRAGMENT_NAME +" onDetach");
        super.onDetach();
    }
}