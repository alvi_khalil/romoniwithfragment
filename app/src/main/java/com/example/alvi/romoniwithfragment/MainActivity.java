package com.example.alvi.romoniwithfragment;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.alvi.romoniwithfragment.Interfaces.FragmentChangeListener;

public class MainActivity extends AppCompatActivity implements FragmentChangeListener {



    private static final String COMMON_TAG = "CombinedLifeCycle";
    private static final String ACTIVITY_NAME = MainActivity.class.getSimpleName();
    private static final String TAG = "dekho";

    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fragmentManager=getSupportFragmentManager();
        Log.d(TAG,""+fragmentManager.getBackStackEntryCount());
        addSwitchFragment();
    }
    private void addSwitchFragment(){



        Fragment fragment;

        fragment = new SwitchUserTypeFragment();

        fragmentTransaction=fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.fragmentContainer,fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
        Log.d(TAG,"add 1st: "+fragmentManager.getBackStackEntryCount());
    }

    @Override
    public void replaceFragment(Fragment fragment) {



        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragmentContainer, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
        Log.d(TAG,"replace: "+fragmentManager.getBackStackEntryCount());
    }

    @Override
    public void onBackPressed() {

        Fragment fragment = fragmentManager.findFragmentById(R.id.fragmentContainer);
        if(fragment!=null && fragmentManager.getBackStackEntryCount()>1){
            fragmentManager.popBackStack();
        }
        else{
            //Toast.makeText(this, "else", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);

        }
        Log.d(TAG,"back: "+fragmentManager.getBackStackEntryCount());
    }
}
