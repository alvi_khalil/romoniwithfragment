package com.example.alvi.romoniwithfragment.Adapters;


import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.alvi.romoniwithfragment.BussinessUser.BusinessUserSignUpFragment;
import com.example.alvi.romoniwithfragment.BussinessUser.SliderBusinessUserFragment;
import com.example.alvi.romoniwithfragment.Interfaces.FragmentChangeListener;
import com.example.alvi.romoniwithfragment.R;

public class SlideAdapter2 extends PagerAdapter {

    Context context;
    LayoutInflater inflater;


    public int[] list_images={
            R.drawable.r5,
            R.drawable.r10,
            R.drawable.r6,
            R.drawable.r7,
            R.drawable.r8,
            R.drawable.r9,
    };

    public String[] list_header={
            "1st page header",
            "2nd page header a big one to test",
            "3rd page header even bigger one to test it to the bone",
            "4th page header",
            "5th page header",
            "6th page header"

    };

    public String[] list_body={
            "1st page body",
            "2nd page body a big one to test",
            "3rd page body even bigger one to test it to the bone",
            "4th page body",
            "5th page body",
            "6th page body"

    };


    public SlideAdapter2(Context context){
        this.context=context;

    }
    @Override
    public int getCount() {
        return list_body.length;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((ConstraintLayout)object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view=inflater.inflate(R.layout.slide2,container,false);
        ConstraintLayout layoutSlide = view.findViewById(R.id.slidelayout);
        ImageView imageView=(ImageView) view.findViewById(R.id.image);
        TextView textView=(TextView) view.findViewById(R.id.textView3);
        TextView textView2=(TextView) view.findViewById(R.id.textView4);
        Button startButton =(Button) view.findViewById(R.id.start_but);
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "ostrichregular.ttf");
        textView.setTypeface(typeface);

        imageView.setImageResource(list_images[position]);
        textView.setText(list_header[position]);
        textView2.setText(list_body[position]);

        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment=new BusinessUserSignUpFragment();
                FragmentChangeListener fc=(FragmentChangeListener)context;
                fc.replaceFragment(fragment);

            }
        });
        if(position==5){
            startButton.setVisibility(View.VISIBLE);
        }
        else
            startButton.setVisibility(View.INVISIBLE);

        container.addView(view);
        return view;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return (view==(ConstraintLayout)object);
    }
}
