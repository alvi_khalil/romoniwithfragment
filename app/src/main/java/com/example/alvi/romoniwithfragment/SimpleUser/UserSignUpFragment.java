package com.example.alvi.romoniwithfragment.SimpleUser;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.alvi.romoniwithfragment.R;


public class UserSignUpFragment extends Fragment {

    private static final String COMMON_TAG = "CombinedLifeCycle";
    private static final String FRAGMENT_NAME = UserLogInFragment.class.getSimpleName();

    private static final String TAG = FRAGMENT_NAME;

    EditText editEmail1,editEmail12,editPass;
    Button buttonFB,buttonContin,back;
    TextView term1,term2,notPost,textOR;
    View v1,v2;
    View view;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.i(TAG, FRAGMENT_NAME +" onAttach");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, FRAGMENT_NAME +" onCreate");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        Log.i(TAG, FRAGMENT_NAME +" onCreateView");
        view =inflater.inflate(R.layout.fragment_user_signup,container,false);
        editEmail1=view.findViewById(R.id.edit_email1);
        editEmail12=view.findViewById(R.id.edit_email2);
        editPass=view.findViewById(R.id.edit_pass);

        buttonFB=view.findViewById(R.id.fb_button);
        buttonContin=view.findViewById(R.id.contin);
        back=view.findViewById(R.id.black_arrow);

        term1=view.findViewById(R.id.textViewterms1);
        term2=view.findViewById(R.id.textViewterms2);
        notPost=view.findViewById(R.id.textView10);
        textOR=view.findViewById(R.id.textView0);


        v1=view.findViewById(R.id.v7);
        v2=view.findViewById(R.id.v8);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        Log.i(TAG, FRAGMENT_NAME +" onActivityCreated");
        super.onActivityCreated(savedInstanceState);
        editEmail1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editEmail1.setVisibility(View.GONE);
                buttonFB.setVisibility(View.GONE);
                term1.setVisibility(View.GONE);
                notPost.setVisibility(View.GONE);
                textOR.setVisibility(View.GONE);
                v1.setVisibility(View.GONE);
                v2.setVisibility(View.GONE);


                editEmail12.setVisibility(View.VISIBLE);
                editPass.setVisibility(View.VISIBLE);
                buttonContin.setVisibility(View.VISIBLE);
                term2.setVisibility(View.VISIBLE);



            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(buttonContin.getVisibility() == View.VISIBLE){
                    editEmail1.setVisibility(View.VISIBLE);
                    buttonFB.setVisibility(View.VISIBLE);
                    term1.setVisibility(View.VISIBLE);
                    notPost.setVisibility(View.VISIBLE);
                    textOR.setVisibility(View.VISIBLE);
                    v1.setVisibility(View.VISIBLE);
                    v2.setVisibility(View.VISIBLE);

                    editEmail1.setText(editEmail12.getText());

                    editEmail12.setVisibility(View.GONE);
                    editPass.setVisibility(View.GONE);
                    buttonContin.setVisibility(View.GONE);
                    term2.setVisibility(View.GONE);
                }
                else{
                    FragmentManager fragmentManager=(FragmentManager)getFragmentManager();
                    fragmentManager.popBackStack();
                }

            }
        });
        buttonContin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editEmail12.getText().toString().equalsIgnoreCase("") || editPass.getText().toString().equalsIgnoreCase("") ) {
                    Toast.makeText(getActivity(), "One or more fields are empty!", Toast.LENGTH_SHORT).show();
                } else {
                    if (!isEmailValid(editEmail12.getText().toString())) {
                        Toast.makeText(getActivity(), "Email format is not Right!", Toast.LENGTH_SHORT).show();

                    } else {
                        if (editPass.getText().toString().length() < 7) {
                            Toast.makeText(getActivity(), "Password is not 6+ characters long!", Toast.LENGTH_SHORT).show();
                        } else {

                            Toast.makeText(getActivity(), "Account Created, Now log in.", Toast.LENGTH_SHORT).show();
                            FragmentManager fragmentManager=(FragmentManager)getFragmentManager();
                            fragmentManager.popBackStack();
                        }
                    }
                }
            }
        });

    }
    boolean isEmailValid(CharSequence email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
    @Override
    public void onStart() {
        Log.i(TAG, FRAGMENT_NAME +" onStart");
        super.onStart();
    }

    @Override
    public void onResume() {
        Log.i(TAG, FRAGMENT_NAME +" onResume");
        super.onResume();
    }

    @Override
    public void onPause() {
        Log.i(TAG, FRAGMENT_NAME +" onPause");
        super.onPause();
    }

    @Override
    public void onStop() {
        Log.i(TAG, FRAGMENT_NAME +" onStop");
        super.onStop();
    }


    @Override
    public void onDestroyView() {
        Log.i(TAG, FRAGMENT_NAME +" onDestroyView");
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        Log.i(TAG, FRAGMENT_NAME +" onDestroy");
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        Log.i(TAG, FRAGMENT_NAME +" onDetach");
        super.onDetach();
    }
}