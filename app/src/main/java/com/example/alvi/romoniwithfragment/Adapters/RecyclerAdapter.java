package com.example.alvi.romoniwithfragment.Adapters;


import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.example.alvi.romoniwithfragment.Classes.GetterSetter;
import com.example.alvi.romoniwithfragment.Interfaces.OnItemClickedFromAdapter;
import com.example.alvi.romoniwithfragment.R;

import java.util.ArrayList;
import java.util.List;


public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.CustomViewHolder> {
    private List<GetterSetter> feedItemList;
    private Context mContext;
    private String check;
    private OnItemClickedFromAdapter mCallback;

    public RecyclerAdapter(Context context, ArrayList<GetterSetter> feedItemList,OnItemClickedFromAdapter listener,String check) {
        this.feedItemList = feedItemList;
        this.mContext = context;
        this.check=check;
        this.mCallback = listener;

    }

    @Override
    public int getItemViewType(int position) {


        if(check.equals("logged")){
            if(position==0){

                return 2;
            }
            else if(position==1)
            {
                return 5;
            }
            else if(position==2)
            {
                return 3;
            }

            else{
                return 1;

            }
        }
        else{
            if(position==0){
                return 4;
            }
            else if(position==1)
            {
                return 2;
            }
            else if(position==2)
            {
                return 5;
            }
            else if(position==3)
            {
                return 3;
            }
            else{
                return 1;

            }

        }

    }

    @Override
    public RecyclerAdapter.CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case 1:
                View viewONE = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_template_one, parent, false);
                CustomViewHolder rowONE = new CustomViewHolder(viewONE,1);
                return rowONE;

            case 2:
                View viewTWO = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_template_two, parent, false);
                CustomViewHolder rowTWO = new CustomViewHolder(viewTWO,2);
                return rowTWO;

            case 3:
                View viewTHREE = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_template_three, parent, false);
                CustomViewHolder rowTHREE = new CustomViewHolder(viewTHREE,3);
                return rowTHREE;
            case 4:
                View viewFour = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_template_four, parent, false);
                CustomViewHolder rowFour = new CustomViewHolder(viewFour,4);
                return rowFour;
            case 5:
                View viewFive = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_template_five, parent, false);
                CustomViewHolder rowFive = new CustomViewHolder(viewFive,5);
                return rowFive;

        }
        return null;
    }


    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {

        GetterSetter dc_list = feedItemList.get(position);

        int num;
        if(check.equals("logged")){

            num=3;

        }

        else{
            num=4;

        }


        if(position==0 && num==4){
            holder.whiteBackGreenSign.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    mCallback.onClick("sign");

                }
            });
            holder.whiteBackGreenLog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    mCallback.onClick("login");


                }
            });
        }

        if(position>=num) {

            holder.image.setImageResource(dc_list.getId());
            Typeface typeface = Typeface.createFromAsset(mContext.getAssets(), "ostrichregular.ttf");
            holder.header.setTypeface(typeface);
            holder.header.setText(dc_list.getHeader());
            holder.body.setText(dc_list.getBody());


            holder.header.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(mContext, "dvbg", Toast.LENGTH_SHORT).show();
                }
            });

        }

    }

    private void setAnimation(FrameLayout container, int position) {
        Animation animation = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left);
        container.startAnimation(animation);
    }

    @Override
    public int getItemCount() {
        return (null != feedItemList ? feedItemList.size() : 0 / 0);
    }

    class CustomViewHolder extends RecyclerView.ViewHolder {



        Button whiteBackGreenSign,whiteBackGreenLog;


        ImageView image;
        TextView header;
        TextView body;


        public CustomViewHolder(View itemView,int item) {
            super(itemView);



            whiteBackGreenSign = itemView.findViewById(R.id.white_back_green_sign);
            whiteBackGreenLog = itemView.findViewById(R.id.white_back_green_log);



            image = itemView.findViewById(R.id.image);
            header = itemView.findViewById(R.id.header);
            body = itemView.findViewById(R.id.body);


        }
    }
}