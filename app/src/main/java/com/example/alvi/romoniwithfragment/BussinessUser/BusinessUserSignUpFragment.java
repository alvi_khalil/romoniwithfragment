package com.example.alvi.romoniwithfragment.BussinessUser;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.alvi.romoniwithfragment.Interfaces.FragmentChangeListener;
import com.example.alvi.romoniwithfragment.R;
import com.example.alvi.romoniwithfragment.SimpleUser.UserLogInFragment;


public class BusinessUserSignUpFragment extends Fragment {

    private static final String COMMON_TAG = "CombinedLifeCycle";
    private static final String FRAGMENT_NAME = UserLogInFragment.class.getSimpleName();

    private static final String TAG = FRAGMENT_NAME;
    EditText editName,editLastName,editEmail,editPass,edirPhone;
    Button creatAccount,backButton;

    View view;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.i(TAG, FRAGMENT_NAME +" onAttach");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, FRAGMENT_NAME +" onCreate");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        Log.i(TAG, FRAGMENT_NAME +" onCreateView");
        view =inflater.inflate(R.layout.fragment_business_user_signup,container,false);
        editName = view.findViewById(R.id.edit_name);
        editLastName = view.findViewById(R.id.edit_last_name);
        editEmail = view.findViewById(R.id.edit_email);
        editPass = view.findViewById(R.id.edit_pass);
        edirPhone = view.findViewById(R.id.edit_phone);
        creatAccount = view.findViewById(R.id.create_button);
        backButton = view.findViewById(R.id.black_arrow);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        Log.i(TAG, FRAGMENT_NAME +" onActivityCreated");
        super.onActivityCreated(savedInstanceState);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager=(FragmentManager)getFragmentManager();
                fragmentManager.popBackStack();
            }
        });
        creatAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (editName.getText().toString().equalsIgnoreCase("")||editLastName.getText().toString().equalsIgnoreCase("") || editEmail.getText().toString().equalsIgnoreCase("") || editPass.getText().toString().equalsIgnoreCase("") || edirPhone.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(getActivity(), "One or more fields are empty!", Toast.LENGTH_SHORT).show();
                } else {
                    if (!isEmailValid(editEmail.getText().toString())) {
                        Toast.makeText(getActivity(), "Email format is not Right!", Toast.LENGTH_SHORT).show();

                    } else {
                        if (editPass.getText().toString().length() < 7) {
                            Toast.makeText(getActivity(), "Password is not 6+ characters long!", Toast.LENGTH_SHORT).show();
                        } else {

                            Toast.makeText(getActivity(), "Account Created, Now log in.", Toast.LENGTH_SHORT).show();
                            Fragment fragment=new UserLogInFragment();
                            FragmentChangeListener fc=(FragmentChangeListener)getActivity();
                            fc.replaceFragment(fragment);
                        }
                    }
                }
            }
        });
    }
    boolean isEmailValid(CharSequence email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
    @Override
    public void onStart() {
        Log.i(TAG, FRAGMENT_NAME +" onStart");
        super.onStart();
    }

    @Override
    public void onResume() {
        Log.i(TAG, FRAGMENT_NAME +" onResume");
        super.onResume();
    }

    @Override
    public void onPause() {
        Log.i(TAG, FRAGMENT_NAME +" onPause");
        super.onPause();
    }

    @Override
    public void onStop() {
        Log.i(TAG, FRAGMENT_NAME +" onStop");
        super.onStop();
    }


    @Override
    public void onDestroyView() {
        Log.i(TAG, FRAGMENT_NAME +" onDestroyView");
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        Log.i(TAG, FRAGMENT_NAME +" onDestroy");
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        Log.i(TAG, FRAGMENT_NAME +" onDetach");
        super.onDetach();
    }
}