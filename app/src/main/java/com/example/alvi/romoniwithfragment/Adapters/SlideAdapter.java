package com.example.alvi.romoniwithfragment.Adapters;


import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.alvi.romoniwithfragment.R;

public class SlideAdapter extends PagerAdapter {

    Context context;
    LayoutInflater inflater;

    public int[] list_images={
            R.drawable.r1,
            R.drawable.r2,
            R.drawable.r3,
            R.drawable.r4
    };

    public String[] list_header={
            "1st page header",
            "2nd page header a big one to test",
            "3rd page header even bigger one to test it to the bone",
            "4th page header",

    };

    public String[] list_body={
            "1st page body",
            "2nd page body a big one to test",
            "3rd page body even bigger one to test it to the bone",
            "4th page body",

    };


    public SlideAdapter(Context context){
        this.context=context;
    }
    @Override
    public int getCount() {
        return list_body.length;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((ConstraintLayout)object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view=inflater.inflate(R.layout.slide,container,false);
        ConstraintLayout layoutSlide = view.findViewById(R.id.slidelayout);
        ImageView imageView=(ImageView) view.findViewById(R.id.image);
        TextView textView=(TextView) view.findViewById(R.id.textView3);
        TextView textView2=(TextView) view.findViewById(R.id.textView4);
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "ostrichregular.ttf");
        textView.setTypeface(typeface);

        imageView.setImageResource(list_images[position]);
        textView.setText(list_header[position]);
        textView2.setText(list_body[position]);
        container.addView(view);
        return view;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return (view==(ConstraintLayout)object);
    }
}
